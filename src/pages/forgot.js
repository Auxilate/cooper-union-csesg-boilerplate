import React from 'react';
import { Link } from 'react-router-dom';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import NavBar from '../components/navbar.js';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import email from '../images/email.svg';
import { compose } from 'redux';
import { connect } from 'react-redux';
import { firebaseConnect, isLoaded, isEmpty } from 'react-redux-firebase';

class ForgotPage extends React.Component{
    render(){
      const top ={
        textAlign: "center",
        margin: '3%',
        fontFamily: 'Raleway',
      }

      const box={
        margin: 'auto',
        height: '25%',
        padding: 'auto',
      }


	return(
	    <div>
      <br />
      <Grid xs={10} sm={6} md={5} lg={4} xl={4} justify="center"  style={box}>
        <Paper elevation={5}>
          <br />
          <h1 style={top}>
  		        Forgot Password?
  		    </h1>
          <form>
            <Grid container spacing={8} justify="center" alignItems="flex-end" >
              <Grid item>
                <img src={email} alt="" />
              </Grid>
              <Grid item>
                <TextField label="Email" />
              </Grid>
            </Grid>

            <br />
            <center>
              <Button variant="raised" color="primary">Submit</Button>
            </center>
          </form>
          <br />
        </Paper>
      </Grid>
      </div>
	)
    }
};

export default ForgotPage
